from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from flaskpage import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

class User(db.Model, UserMixin):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(100), unique=True, nullable=False)
	email = db.Column(db.String(100), unique=True, nullable=False)
	password = db.Column(db.String(100), nullable=False)
	pages = db.relationship('Pages', backref='author', lazy=True)
	last_seen = db.Column(db.DateTime, nullable=False, default=datetime(1, 1, 1, 0, 0))

	def get_reset_token(self, expires_sec=1800):
		s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
		return s.dumps({'user_id': self.id}).decode('utf-8')

	@staticmethod
	def verify_reset_token(token):
		s = Serializer(current_app.config['SECRET_KEY'])
		try:
			user_id = s.loads(token)['user_id']
		except:
			return None
		return User.query.get(user_id)

	def __repr__(self):
		return f"User('{self.id}', '{self.username}', '{self.email}', '{self.last_seen}')"

	def serialize(self):
		return {
			'username': self.username
		}

class Pages(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)
	name = db.Column(db.Text, index=True)
	content = db.relationship('Elements', backref='page', lazy=True)
	access_mode = db.Column(db.Integer, nullable=False, default=0)
	last_ver = db.Column(db.DateTime, nullable=False, index=True, default=datetime.now())

	def __repr__(self):
		return f"Homework('{self.id}', '{self.user_id}', '{self.name}', '{self.content}')"

	def serialize(self):
		return {
			'id': self.id,
			'user_id': self.user_id,
			'name': self.name,
			'access_mode': self.access_mode,
			'last_ver': self.last_ver.timestamp()*1000,
			'author': self.author.username
		}

class Elements(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	page_id = db.Column(db.Integer, db.ForeignKey('pages.id'), nullable=False, index=True)
	data = db.Column(db.Text, nullable=False)
	content = db.Column(db.Text)
	last_mod = db.Column(db.DateTime, nullable=False, index=True, default=datetime.now())

	def __repr__(self):
		return f"Homework('{self.id}', '{self.page_id}', '{self.data}', '{self.content}', '{self.last_mod}')"

	def serialize(self):
		return {
			'id': self.id,
			'page_id': self.page_id,
			'data': self.data,
			'content': self.content,
			'last_mod': self.last_mod.timestamp()*1000
		}