import os
import secrets
from PIL import Image
from flask import url_for, current_app
from flask_mail import Message
from flaskpage import mail

def send_reset_email(user):
	token = user.get_reset_token()
	msg = Message('Password Reset Request',
				  sender='noreply@va.lv',
				  recipients=[user.email])
	msg.body = f'''A request has bin sent to reset your password.
To reset your password visit the following link:
{url_for('users.reset_token', token=token, _external=True)}
If you did not make this request. Ignore this email and no changes will be made.
'''
	mail.send(msg)
