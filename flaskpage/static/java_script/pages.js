function setup() {
	updateCycle = -1
	mouseRel = false
	allPages = []
	scrollY = 0
	windowX = windowWidth
	windowY = windowHeight
	createCanvas(windowX, windowY)
	textSize(12)
	textFont('Helvetica')
	noStroke()
	fill(0)
	textAlign(CENTER)
	loadPages()
}

function draw() {
	date = new Date()
	background(120)
	drawPages()
	strokeWeight(2)
	stroke(0)
	if (scrollY > 0) {
		scrollY /= 1.05
	}
	if (scrollY < -allPages.length*75+windowY-20) {
		scrollY = (scrollY+allPages.length*75-windowY+20)/1.05-allPages.length*75+windowY-20
	}
	if (scrollY > -0.0001 && scrollY < 0.0001) {
		scrollY = 0
	}
	if (8 <= mouseX && mouseX <= 59 && 8 <= mouseY && mouseY <= 59) {
		fill(150, 200)
		if (mouseRel == true) {
			addPage()
		}
	} else {
		fill(111, 200)
	}
	rect(9, 9, 50)
	if (8 <= mouseX && mouseX <= 59 && windowY-121 <= mouseY && mouseY <= windowY-70) {
		fill(150, 200)
		if (mouseRel == true) {
			window.location.replace(accountURL)
		}
	} else {
		fill(111, 200)
	}
	rect(9, windowY-120, 50)
	if (8 <= mouseX && mouseX <= 59 && windowY-61 <= mouseY && mouseY <= windowY-10) {
		fill(150, 200)
		if (mouseRel == true) {
			window.location.replace(logoutURL)
		}
	} else {
		fill(111, 200)
	}
	rect(9, windowY-60, 50)

	drawAccountLogo(14, windowY-115)
	drawAddLogo(18, 18)
	drawLogoutLogo(15, windowY-53)
	mouseRel = false
}

function drawAccountLogo(x, y) {
	noStroke()
	fill(0)
	circle(x+20, y+10, 20)
	arc(x+20, y+40, 40, 40, PI, TWO_PI);
}

function drawGearLogo(x, y) {
	stroke(0)
	noFill()
	strokeWeight(10)
	circle(x+22, y+22, 16)
	fill(0)
	noStroke()
	xy = [x+22, y+22]
	bb = [4.5, 17]
	rr = [9, 15]
	ll = [1, 12]
	tt = [7, 8]
	quad(xy[0]+ll[0], xy[1]+ll[1], xy[0]+tt[0], xy[1]+tt[1], xy[0]+rr[0], xy[1]+rr[1], xy[0]+bb[0], xy[1]+bb[1])
	quad(xy[0]-ll[0], xy[1]-ll[1], xy[0]-tt[0], xy[1]-tt[1], xy[0]-rr[0], xy[1]-rr[1], xy[0]-bb[0], xy[1]-bb[1])
	quad(xy[0]-ll[0], xy[1]+ll[1], xy[0]-tt[0], xy[1]+tt[1], xy[0]-rr[0], xy[1]+rr[1], xy[0]-bb[0], xy[1]+bb[1])
	quad(xy[0]+ll[0], xy[1]-ll[1], xy[0]+tt[0], xy[1]-tt[1], xy[0]+rr[0], xy[1]-rr[1], xy[0]+bb[0], xy[1]-bb[1])
	bb.reverse()
	rr.reverse()
	ll.reverse()
	tt.reverse()
	quad(xy[0]+ll[0], xy[1]+ll[1], xy[0]+tt[0], xy[1]+tt[1], xy[0]+rr[0], xy[1]+rr[1], xy[0]+bb[0], xy[1]+bb[1])
	quad(xy[0]-ll[0], xy[1]-ll[1], xy[0]-tt[0], xy[1]-tt[1], xy[0]-rr[0], xy[1]-rr[1], xy[0]-bb[0], xy[1]-bb[1])
	quad(xy[0]-ll[0], xy[1]+ll[1], xy[0]-tt[0], xy[1]+tt[1], xy[0]-rr[0], xy[1]+rr[1], xy[0]-bb[0], xy[1]+bb[1])
	quad(xy[0]+ll[0], xy[1]-ll[1], xy[0]+tt[0], xy[1]-tt[1], xy[0]+rr[0], xy[1]-rr[1], xy[0]+bb[0], xy[1]-bb[1])
}

function addPage() {
	$.post("/pages"+pageID, JSON.stringify(["new"]), function(data) {
		allPages.push(data)
	})
}

function loadPages() {
	$.post("/pages", JSON.stringify(["load"]), function(data) {
		for (page of data.obj) {
			//print(page)
			allPages.push(page)
		}
		updateCycle = 0
	})
}

function drawPages() {
	fill(255, 200)
	noStroke()
	rect(4, 4, 60, windowY-9)
	i = 0
	for (page of allPages) {
		if (windowX < 540) {
			drawPage(70, 25+75*i+scrollY, page)
		} else {
			drawPage(windowX/2-200, 25+75*i+scrollY, page)
		}
		i++
	}
}

function drawPage(x, y, page) {
	strokeWeight(2)
	stroke(0)
	if (x <= mouseX+1 && mouseX <= x+400 && y <= mouseY+1 && mouseY <= y+50) {
		fill(150, 200)
		if (mouseRel == true) {
			window.location.replace(pagesURL.slice(0,-1)+"/"+page.id)
		}
	} else {
		fill(111, 200)
	}
	rect(x, y, 400, 50)
	textSize(24)
	textFont('Helvetica')
	noStroke()
	fill(0)
	textAlign(LEFT)
	if (page.name == null) {
		text("Unnamed", x+5, y+23)
	} else {
		text(page.name, x+5, y+23)
	}
	textSize(16)
	text(page.author, x+5, y+43)
	textAlign(RIGHT)
	dateTime = new Date()
	dateTime.setTime(page.last_ver)
	text(dateTime.toLocaleString('en-GB'), x+395, y+43)
}

function drawAddLogo(x, y) {
	fill(0)
	noStroke()
	rect(x+12, y, 8, 32)
	rect(x, y+12, 32, 8)
}

function drawLogoutLogo(x, y) {
	stroke(0)
	strokeWeight(3)
	line(x, y+18, x+8, y+10)
	line(x, y+18, x+25, y+18)
	line(x, y+18, x+8, y+26)
	line(x+14, y+1, x+14, y+10)
	line(x+14, y+26, x+14, y+35)
	line(x+33, y+36, x+15, y+36)
	line(x+33, y, x+15, y)
	line(x+34, y+35, x+34, y+1)
}

function resizeWin() {
	windowX = windowWidth
	windowY = windowHeight
	createCanvas(windowX, windowY)
}

function mouseReleased() {
	if (mouseButton === LEFT) {
		mouseRel = true
	}
}

function mouseWheel(event) {
	scrollY -= event.delta*5
  }