function pingUpdates() {
	updateCycle++
	message = ["refresh", allElements.map(x=>x[4])]
	$.post("/page/"+pageID, JSON.stringify(message), function(data) {
		if (data.obj.length) {
			//print("–––––––––––––––––––––––––––––––––––––––")
		}
		for (o of data.obj) {
			o.data = JSON.parse(o.data)
			element = allElements[o.id]
			print(o.id+" "+o.last_mod+" "+element)
			ffgg = [element, o]
			if (element === undefined) {
				if (o.content === null || o.content == "") {
					allElements.push([o.data.type, o.data.x, o.data.y, undefined, o.last_mod])
				} else if (o.data.type == 0) {
					allElements.push([o.data.type, o.data.x, o.data.y, o.content, o.last_mod])
				} else if (o.data.type == 1) {
					allElements.push([o.data.type, o.data.x, o.data.y, createImg(o.content, '').hide(), o.last_mod])
				}
			} else if (o.data.type == -1) {
				element[4] = o.last_mod
				element[0] = -1
				element[3] = undefined
			} else if (o.last_mod > element[4]) {
				element[4] = o.last_mod
				element[1] = o.data.x
				element[2] = o.data.y
				if (o.content === null) {
					element[3] = undefined
				} else if (o.content != "") {
					if (o.data.type == 0) {
						element[3] = o.content
					} else if (o.data.type == 1) {
						element[3] = createImg(o.content, '').hide()
					}
				}
			}
			ggff = [element, o]
		}
		updateCycle--
	})
}

function getTitle() {
	updateCycle++
	$.post("/page/"+pageID, JSON.stringify(["getTitle"]), function(respons) {
		title = respons
		updateCycle--
	})
}

function setTitle(name) {
	updateCycle++
	$.post("/page/"+pageID, JSON.stringify(["setTitle", name]), function(respons) {
		updateCycle--
	})
}

function sendElement(id, data) {
	updateCycle++
	message = ["send", ...data, id]
	$.post("/page/"+pageID, JSON.stringify(message), function(respons) {
		updateCycle--
	})
}

function modElement(id, data) {
	updateCycle++
	message = ["update", data[0], data[1], data[2], id, data[4]]
	$.post("/page/"+pageID, JSON.stringify(message), function(respons) {
		updateCycle--
	})
}

function getPubl() {
	updateCycle++
	$.post("/page/"+pageID, JSON.stringify(["getPubl"]), function(respons) {
		private = respons
		updateCycle--
	})
}

function setPubl(value) {
	updateCycle++
	message = ["setPubl", value]
	$.post("/page/"+pageID, JSON.stringify(message), function(respons) {
		updateCycle--
	})
}

function loadElements() {
	$.post("/page/"+pageID, JSON.stringify(["load"]), function(data) {
		i = 0
		for (o of data.obj) {
			o.data = JSON.parse(o.data)
			while (o.id != i) {
				allElements.push([-1, 0, 0, undefined, 946677600000])
				i++
			}
			if (o.content === null) {
				allElements.push([o.data.type, o.data.x, o.data.y, undefined, o.last_mod])
			} else if (o.data.type == 0) {
				allElements.push([o.data.type, o.data.x, o.data.y, o.content, o.last_mod])
			} else if (o.data.type == 1) {
				allElements.push([o.data.type, o.data.x, o.data.y, createImg(o.content, '').hide(), o.last_mod])
			}
			i++
		}
		updateCycle = 0
		getPubl()
		getTitle()
	})
}

function handleFile(file) {
	if (file.type === 'image') {
		allElements[selected_element][3] = file.data
		allElements[selected_element][4] = date.getTime()
		sendElement(selected_element, allElements[selected_element])
		allElements[selected_element][3] = createImg(file.data, '');
		allElements[selected_element][3].hide()
	} else {
		allElements[selected_element][3] = undefined
		allElements[selected_element][4] = date.getTime()
		sendElement(selected_element, allElements[selected_element])
	}
}