function mouseReleased() {
	if (mouseButton === LEFT) {
		mouseRel = true
		if (draging == true) {
			draging = false
			if (mouseX <= 63 && selected_element != -1) {
				element = allElements[selected_element]
				element[0] = -1
				element[3] = undefined
				element[4] = 946677600000
				addDeleteRequest(selected_element, element)
				selected_element = -1
			}
		}
	}
	if (mouseButton === CENTER) {
		//draging = false
	}
	if (mouseButton === RIGHT) {
		selectedTool = -1
	}
}

function mouseDragged() {
	if (selected_element != -1 && mouseButton === LEFT) {
		if (!draging && mouseX > 63 || draging) {
			draging = true
			element = allElements[selected_element]
			element[1] += movedX/zoom
			element[2] += movedY/zoom
			element[4] = date.getTime()
			modElement(selected_element, element)
		}
	}
}

function keyPressed() {
	if (selected_element != -1 || options) {
		if (options) {
			element = [0, 0, 0, title, 0]
		} else {
			element = allElements[selected_element]
		}
		if (element[0] == 0) {
			if (element[3] == undefined) {
				element[3] = ""
			}
			if (key.length == 1) {
				element[3] += key
			} else if (key == 'Backspace') {
				element[3] = element[3].slice(0, -1)
				eraseTime = date.getTime()
				eraseTogle = true
			} else if (key == 'Enter') {
				element[3] += '\n';
			}
			if (element[3] == "") {
				element[3] = undefined
			}
			element[4] = date.getTime()
			if (options) {
				title = element[3]
			} else {
				sendElement(selected_element, element)
			}
		}
	}
}

function resizeWin() {
	windowX = windowWidth
	windowY = windowHeight
	createCanvas(windowX, windowY)
}
