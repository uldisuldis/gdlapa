function setup() {
	createFileInput(handleFile)
	$("input").css("position", "absolute")
	$("input").css("top", "-100px")
	$("input").attr("id", "input-file")
	$("button").css("display", "none")
	$("button").click(function() {
		$("#input-file").click()
	})
	title = ""
	mouseRel = false
	selectedTool = -1
	allElements = []
	worldX = 0
	worldY = 0
	zoom = 1
	selected_element = -1
	eraseTogle = false
	eraseTime = 0
	draging = false
	updateCycle = -1
	windowX = windowWidth
	windowY = windowHeight
	deleteRequests = []
	options = false
	private = 0
	createCanvas(windowX, windowY)
	loadElements()
}

function draw() {
	date = new Date()
	if (mouseRel && mouseX > 63 && selectedTool >= 0) {
		placeElement(selectedTool)
	}
	if (mouseIsPressed && mouseButton === CENTER) {
		worldX += movedX/zoom
		worldY += movedY/zoom
	}
	if (mouseIsPressed && mouseButton === LEFT && selectedTool == -1) {
		selectElement()
	}
	if (!keyIsDown(BACKSPACE)) {
		if (eraseTogle) {
			if (selected_element != -1) {
				element = allElements[selected_element]
				if (element[0] == 0) {
					element[4] = date.getTime()
					sendElement(selected_element, element)
				}
			}
		}
		eraseTogle = false
	}
	background(120)
	if (updateCycle != -1) {
		//print(updateCycle)
		if (!options) {
			updateElements()
			drawElements()
		} else {
			if (eraseTogle && date.getTime() - eraseTime > 500) {
				title = title.slice(0, -1)
			}
			drawOptions()
		}
		if (selectedTool == 0) {
			drawTextLogo(mouseX, mouseY+15)
		}
		if (selectedTool == 1) {
			drawImageLogo(mouseX-20, mouseY-15)
		}
		drawGUI()
		if (updateCycle == 0 && deleteRequests.length > 0) {
			sendElement(...deleteRequests.pop())
		}
		if (updateCycle == 0) {
			pingUpdates()
		}
		if (draging && mouseX <= 63) {
			drawTrashLogo(mouseX-20, mouseY-25)
		}
	}
	mouseRel = false
}

function addDeleteRequest(selected_element, element) {
	deleteRequests.push([selected_element, element])
}

function placeElement(tool_num) {
	element = [tool_num, (mouseX - worldX*zoom - windowX/2)/zoom, (mouseY - worldY*zoom - windowY/2)/zoom, undefined, date.getTime()]
	modElement(allElements.length, element)
	allElements.push(element)
	if (!keyIsDown(SHIFT)) {
		selectedTool = -1
	}
}

function updateElements() {
	if (selected_element != -1) {
		element = allElements[selected_element]
		if (element[0] == 0 && eraseTogle && date.getTime() - eraseTime > 500) {
			allElements[selected_element][3] = element[3].slice(0, -1)
		}
	}
	imageID = 0
	for (element of allElements) {
		if (element[0] == 1 && !draging && selectedTool == -1 && mouseX > 63) {
			if (element[3] == undefined) {
				if (inSquare(element, 44, 34)) {
					$("button").css("display", "block")
					$("button").css("left", mouseX-50)
					$("button").css("top", mouseY-50)
					break
				}
			} else {
				if (inImage(element)) {
					$("button").css("display", "block")
					$("button").css("left", mouseX-50)
					$("button").css("top", mouseY-50)
					break
				}
			}
		}
		imageID++
	} 
	if (imageID == allElements.length) {
		$("button").css("display", "none")
	}
}

function selectElement() {
	i = 0
	passCheck = false
	if (selected_element != -1) {
		element = allElements[selected_element]
		if (element[0] == 0) {
			passCheck = !inSquare(element, 30, 30)
		} else if (element[0] == 1) {
			if (element[3] == undefined) {
				passCheck = !inSquare(element, 44, 34)
			} else {
				passCheck = !inImage(element)
			}
		}
	}
	if (selected_element == -1 || passCheck) {
		for (element of allElements) {
			if (element[0] == 0) {
				if (inSquare(element, 30, 30)) {
					selected_element = i
					i = -1
					break
				}
			} else if (element[0] == 1) {
				if (element[3] == undefined) {
					if (inSquare(element, 44, 34)) {
						selected_element = i
						i = -1
						break
					}
				} else {
					if (inImage(element)) {
						selected_element = i
						i = -1
						break
					}
				}
			}
			i++
		}
		if (i != -1) {
			selected_element = -1
		}
	}
}

function inImage(element) {
	imgX = element[3].width/2*zoom
	imgY = element[3].height/2*zoom
	return inSquare(element, imgX, imgY)
}

function inSquare(element, w, h) {
	w /= 2
	h /= 2
	absX = mouseX-worldX
	absY = mouseY-worldY
	//return absX-element[1]-windowX/2 < w && absX-element[1]-windowX/2 > -w && absY-element[2]-windowY/2 < h && absY-element[2]-windowY/2 > -h
	return mouseX < (worldX+element[1])*zoom+w+windowX/2 && mouseX > (worldX+element[1])*zoom-w+windowX/2 && mouseY < (worldY+element[2])*zoom+h+windowY/2 && mouseY > (worldY+element[2])*zoom-h+windowY/2
}

function mouseWheel(event) {
	if (event.delta > 0) {
		zoom /= (1+event.delta/10)
	} else if (event.delta < 0) {
		zoom *= (1-event.delta/10)
	}
}
