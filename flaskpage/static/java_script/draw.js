function drawGUI() {
	fill(255, 200)
	noStroke()
	if (options) {
		rect(4, windowY-125, 60, 120)
	} else {
		rect(4, 4, 60, windowY-9)
	}
	strokeWeight(2)
	stroke(0)
	if (!options) {
		for (i = 0; i < 2; i++) {
			if (8 <= mouseX && mouseX <= 59 && 8+i*56 <= mouseY && mouseY <= 59+i*56) {
				if (selectedTool == i) {
					fill(0, 255, 0, 200)
				} else {
					fill(150, 200)
				}
				if (mouseRel == true) {
					selectedTool = i
				}
			} else if (selectedTool == i) {
				fill(0, 222, 0, 200)
			} else {
				fill(111, 200)
			}
			rect(9, 9+i*56, 50)
		}
		drawTextLogo(34, 48)
		drawImageLogo(14, 75)
	}
	if (keyIsDown(SHIFT)) {
		if (8 <= mouseX && mouseX <= 59 && windowY-121 <= mouseY && mouseY <= windowY-70) {
			fill(150, 200)
			if (mouseRel == true) {
				window.location.replace(accountURL)
			}
		} else {
			fill(111, 200)
		}
		rect(9, windowY-120, 50)
		if (8 <= mouseX && mouseX <= 59 && windowY-61 <= mouseY && mouseY <= windowY-10) {
			fill(150, 200)
			if (mouseRel == true) {
				window.location.replace(logoutURL)
			}
		} else {
			fill(111, 200)
		}
		rect(9, windowY-60, 50)
		drawAccountLogo(14, windowY-115)
		drawLogoutLogo(15, windowY-53)
	} else {
		if (isCreator == "True") {
			if (8 <= mouseX && mouseX <= 59 && windowY-121 <= mouseY && mouseY <= windowY-70) {
				fill(150, 200)
				if (mouseRel == true) {
					if (options) {
						setTitle(title)
						setPubl(private)
					}
					options = !options
				}
			} else {
				fill(111, 200)
			}
			rect(9, windowY-120, 50)
			drawGearLogo(12, windowY-117)
		}
		strokeWeight(2)
		stroke(0)
		if (8 <= mouseX && mouseX <= 59 && windowY-61 <= mouseY && mouseY <= windowY-10) {
			fill(150, 200)
			if (mouseRel == true) {
				window.location.replace(pagesURL)
				if (options) {
					setTitle(title)
					setPubl(private)
				}
			}
		} else {
			fill(111, 200)
		}
		rect(9, windowY-60, 50)
		drawBackLogo(14, windowY-50)
	}

}

function drawOptions() {
	strokeWeight(2)
	stroke(0)
	boxX = max((windowX-400)/2, 5)
	if (windowX < 410) {
		if (boxX-1 <= mouseX && mouseX <= boxX+windowX-10 && 49 <= mouseY && mouseY <= 100) {
			fill(150, 200)
		} else {
			fill(111, 200)
		}
		rect(boxX, 50, windowX-10, 50)
		if (boxX-1 <= mouseX && mouseX <= boxX+50 && 119 <= mouseY && mouseY <= 170) {
			fill(150, 200)
			if (mouseRel == true) {
				private = !private
			}
		} else {
			fill(111, 200)
		}
		rect(boxX, 120, 50)
		if (private) {
			fill(255)
			stroke(0)
			strokeWeight(10)
			line(boxX+12, 132, boxX+38, 158)
			line(boxX+38, 132, boxX+12, 158)
		}
	} else {
		if (boxX <= mouseX && mouseX <= boxX+400 && 49 <= mouseY && mouseY <= 100) {
			fill(150, 200)
		} else {
			fill(111, 200)
		}
		rect(boxX, 50, 400, 50)
		if (boxX <= mouseX && mouseX <= boxX+50 && 119 <= mouseY && mouseY <= 170) {
			fill(150, 200)
			if (mouseRel == true) {
				private = !private
			}
		} else {
			fill(111, 200)
		}
		rect(boxX, 120, 50)
		if (private) {
			fill(255)
			stroke(0)
			strokeWeight(10)
			line(boxX+12, 132, boxX+38, 158)
			line(boxX+38, 132, boxX+12, 158)
		}
	}
	noStroke()
	fill(0)
	textSize(30)
	textFont('Georgia')
	textAlign(LEFT)
	text("Page name:", boxX, 40)
	text("Private", boxX+60, 154)
	textFont('Helvetica')
	textSize(36)
	if (title != null) {
		text(title, boxX+8, 85)
		drawCursor(title)
	} else {
		drawCursor("")
	}
}

function drawAccountLogo(x, y) {
	noStroke()
	fill(0)
	circle(x+20, y+10, 20)
	arc(x+20, y+40, 40, 40, PI, TWO_PI);
}

function drawGearLogo(x, y) {
	stroke(0)
	noFill()
	strokeWeight(10)
	circle(x+22, y+22, 16)
	fill(0)
	noStroke()
	xy = [x+22, y+22]
	bb = [4.5, 17]
	rr = [9, 15]
	ll = [1, 12]
	tt = [7, 8]
	quad(xy[0]+ll[0], xy[1]+ll[1], xy[0]+tt[0], xy[1]+tt[1], xy[0]+rr[0], xy[1]+rr[1], xy[0]+bb[0], xy[1]+bb[1])
	quad(xy[0]-ll[0], xy[1]-ll[1], xy[0]-tt[0], xy[1]-tt[1], xy[0]-rr[0], xy[1]-rr[1], xy[0]-bb[0], xy[1]-bb[1])
	quad(xy[0]-ll[0], xy[1]+ll[1], xy[0]-tt[0], xy[1]+tt[1], xy[0]-rr[0], xy[1]+rr[1], xy[0]-bb[0], xy[1]+bb[1])
	quad(xy[0]+ll[0], xy[1]-ll[1], xy[0]+tt[0], xy[1]-tt[1], xy[0]+rr[0], xy[1]-rr[1], xy[0]+bb[0], xy[1]-bb[1])
	bb.reverse()
	rr.reverse()
	ll.reverse()
	tt.reverse()
	quad(xy[0]+ll[0], xy[1]+ll[1], xy[0]+tt[0], xy[1]+tt[1], xy[0]+rr[0], xy[1]+rr[1], xy[0]+bb[0], xy[1]+bb[1])
	quad(xy[0]-ll[0], xy[1]-ll[1], xy[0]-tt[0], xy[1]-tt[1], xy[0]-rr[0], xy[1]-rr[1], xy[0]-bb[0], xy[1]-bb[1])
	quad(xy[0]-ll[0], xy[1]+ll[1], xy[0]-tt[0], xy[1]+tt[1], xy[0]-rr[0], xy[1]+rr[1], xy[0]-bb[0], xy[1]+bb[1])
	quad(xy[0]+ll[0], xy[1]-ll[1], xy[0]+tt[0], xy[1]-tt[1], xy[0]+rr[0], xy[1]-rr[1], xy[0]+bb[0], xy[1]-bb[1])
}

function drawTextLogo(x, y) {
	noStroke()
	fill(0)
	textSize(40)
	textFont('Georgia')
	textAlign(CENTER)
	text("T", x, y)
}

function drawImageLogo(x, y) {
	fill(0)
	noStroke()
	triangle(x+1, y+19, x+15, y+19, x+8, y+12)
	quad(x+16, y+19, x+39, y+19, x+39, y+14, x+30, y+5)
	rect(x+1, y+19, 38, 7)
	rect(x+4, y+26, 32, 3)
	circle(x+4, y+26, 7)
	circle(x+36, y+26, 7)
	circle(x+16, y+7, 7)
	noFill()
	stroke(0)
	strokeWeight(2)
	rect(x, y, 40, 30, 5)
}

function drawBackLogo(x, y) {
	fill(0)
	noStroke()
	triangle(x, y+15, x+15, y+30, x+15, y)
	rect(x+15, y+11, 25, 8)
}

function drawLogoutLogo(x, y) {
	stroke(0)
	strokeWeight(3)
	line(x, y+18, x+8, y+10)
	line(x, y+18, x+25, y+18)
	line(x, y+18, x+8, y+26)
	line(x+14, y+1, x+14, y+10)
	line(x+14, y+26, x+14, y+35)
	line(x+33, y+36, x+15, y+36)
	line(x+33, y, x+15, y)
	line(x+34, y+35, x+34, y+1)
}

function drawTrashLogo(x, y) {
	fill(255, 0, 0)
	noStroke()
	rect(x+12, y, 16, 8, 4)
	rect(x, y+4, 40, 4, 2)
	noFill()
	stroke(255, 0, 0)
	strokeWeight(4)
	rect(x+8, y+14, 8, 30, 4)
	rect(x+16, y+14, 8, 30, 4)
	rect(x+24, y+14, 8, 30, 4)
	rect(x+6, y+12, 28, 34, 0, 0, 4)
}

function drawElements() {
	i = 0
	for (element of allElements) {
		if (selected_element != i || mouseX > 63 || !draging) {
			if (element[0] == 0) {
				if (element[3] == undefined) {
					if (selected_element != i) {
						drawTextLogo((element[1]+worldX)*zoom+windowX/2, (element[2]+15+worldY)*zoom+windowY/2)
					}
				} else {
					noStroke()
					fill(0)
					textSize(30*zoom)
					textFont('Helvetica')
					textAlign(CENTER)
					text(element[3], (element[1]+worldX)*zoom+windowX/2, (element[2]+9+worldY)*zoom+windowY/2)
				}
				if (selected_element == i) {
					drawTextCursor(element)
				}
			}
			if (element[0] == 1) {
				if (element[3] == undefined) {
					drawImageLogo((element[1]+worldX)*zoom-20+windowX/2, (element[2]+worldY)*zoom-15+windowY/2)
				} else {
					try {
						image(element[3], (element[1]+worldX-element[3].width/4)*zoom+windowX/2, (element[2]+worldY-element[3].height/4)*zoom+windowY/2, element[3].width/2*zoom, element[3].height/2*zoom)
					} catch (err) {
						print("Image Draw Error")
					}
				}
			}
		}
		i++
	}
}

function drawTextCursor(element) {
	if (element[0] == 0 && date.getTime() % 800 < 500) {
		stroke(0)
		strokeWeight(2)
		newLineNum = 0
		if (element[3] == undefined) {
			textX = 0
		} else {
			textX = Math.trunc(textWidth(element[3].split("\n").pop())/2) + 2
			newLineNum = matchAll(element[3], '\n').length
		}
		line((element[1]+worldX)*zoom+windowX/2+textX, (element[2]-15+worldY+newLineNum*37.5)*zoom+windowY/2, (element[1]+worldX)*zoom+windowX/2+textX, (element[2]+15+worldY+newLineNum*37.5)*zoom+windowY/2)
	}
}

function drawCursor(text) {
	if (date.getTime() % 800 < 500) {
		stroke(0)
		strokeWeight(2)
		textSize(36)
		textX = textWidth(text)
		if (windowX < 410) {
			line(textX+14, 60, textX+14, 90)
		} else {
			line((windowX-410)/2+textX+14, 60, (windowX-410)/2+textX+14, 90)
		}
	}
}
