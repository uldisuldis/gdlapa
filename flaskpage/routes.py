from datetime import datetime
from sqlalchemy import or_
from flask import render_template, url_for, flash, abort, redirect, request, Blueprint, jsonify
from flask_login import login_user, current_user, logout_user, login_required
from flaskpage import db, bcrypt
from flaskpage.models import User, Elements, Pages
from flaskpage.utils import send_reset_email
from flaskpage.forms import RegistrationForm, LoginForm, UpdateAccountForm, RequestResetForm, ResetPasswordForm

usersBP = Blueprint('users', __name__)
mainBP = Blueprint('main', __name__)
errorsBP = Blueprint('errors', __name__)

@mainBP.route("/page/<int:page_id>", methods=['GET'])
@login_required
def page(page_id):
	creator = (current_user.id == Pages.query.get(page_id).author.id)
	print(current_user.id)
	print(Pages.query.get(page_id).author.id)
	print(creator)
	return render_template('page.html', page_id=page_id, creator=creator)

@mainBP.route("/page/<int:page_id>", methods=['POST'])
@login_required
def page_io(page_id):
	data = request.get_json(force=True)
	if data[0] == 'refresh':
		query = Elements.query.filter_by(page_id=page_id).all()
		elements = []
		for i in range(len(data[1])):
			if not Elements.query.get(i) and data[1][i] != 946677600000:
				data_string = "{\"type\": -1, \"x\": 0, \"y\": 0, \"img_date\": -1}"
				element = Elements(id=i, page_id=page_id, data=data_string, content=None, last_mod=datetime(2000, 1, 1))
				elements.append(element)
		for element in query:
			new_element = element.id >= len(data[1])
			if new_element or element.last_mod.timestamp()*1000 > data[1][element.id]:
				if not new_element and int(element.data.split(" ")[1].split(",")[0]) == 1 and int(element.data.split(" ")[7].split("}")[0]) < data[1][element.id]:
					element.content = ""
				if not new_element and int(element.data.split(" ")[7].split("}")[0]) < data[1][element.id]:
					element.content = ""
				elements.append(element)
		return jsonify(obj=[element.serialize() for element in elements])
	if data[0] == 'text':
		search_id = data[1]
		return str(User.query.filter(User.id > search_id).all())
	elif data[0] == "send":
		if current_user != Pages.query.get(page_id).author and Pages.query.get(page_id).access_mode == 1:
			abort(403)
		element = Elements.query.get(data[6])
		if element:
			db.session.delete(element)
		if data[1] != -1:
			data_string = "{\"type\": "+str(data[1])+", \"x\": "+str(data[2])+", \"y\": "+str(data[3])+", \"img_date\": "+str(data[5])+"}"
			element = Elements(id=data[6], page_id=page_id, data=data_string, content=data[4], last_mod=datetime.fromtimestamp(data[5]/1000))
			if element.last_mod > datetime.now():
				element.last_mod = datetime.now()
			db.session.add(element)
		page = Pages.query.get(page_id)
		page.last_ver = datetime.now()
		db.session.commit()
		return "OK"
	elif data[0] == "load":
		if current_user != Pages.query.get(page_id).author and Pages.query.get(page_id).access_mode == 1:
			abort(403)
		return jsonify(obj=[element.serialize() for element in Elements.query.filter_by(page_id=page_id).all()])
	elif data[0] == "getTitle":
		return jsonify(Pages.query.get(page_id).name)
	elif data[0] == "setTitle":
		page = Pages.query.get(page_id)
		if current_user != page.author:
			abort(403)
		page.name = data[1]
		page.last_ver = datetime.now()
		db.session.commit()
		return "OK set"
	elif data[0] == "getPubl":
		return jsonify(Pages.query.get(page_id).access_mode)
	elif data[0] == "setPubl":
		page = Pages.query.get(page_id)
		if current_user != page.author:
			abort(403)
		page.access_mode = data[1]
		page.last_ver = datetime.now()
		db.session.commit()
		return "OK set"
	elif data[0] == "update":
		if current_user != Pages.query.get(page_id).author and Pages.query.get(page_id).access_mode == 1:
			abort(403)
		element = Elements.query.get(data[4])
		if element:
			img_data = element.data.split(" ")[7].split("}")[0]
			element.data = "{\"type\": "+str(data[1])+", \"x\": "+str(data[2])+", \"y\": "+str(data[3])+", \"img_date\": "+img_data+"}"
			element.last_mod = datetime.fromtimestamp(data[5]/1000)
			page = Pages.query.get(page_id)
			page.last_ver = datetime.now()
			db.session.commit()
			return "OK edit"
		else:
			data_string = "{\"type\": "+str(data[1])+", \"x\": "+str(data[2])+", \"y\": "+str(data[3])+", \"img_date\": "+str(data[5])+"}"
			element = Elements(id=data[4], page_id=page_id, data=data_string, last_mod=datetime.fromtimestamp(data[5]/1000))
			if element.last_mod > datetime.now():
				element.last_mod = datetime.now()
			db.session.add(element)
			page = Pages.query.get(page_id)
			page.last_ver = datetime.now()
			db.session.commit()
			return "OK new"
	return "Invalid Request"

@mainBP.route("/pages", methods=['GET'])
@login_required
def pages():
	return render_template('pages.html')

@mainBP.route("/pages", methods=['POST'])
@login_required
def pages_io():
	data = request.get_json(force=True)
	if data[0] == "load":
		return jsonify(obj=[page.serialize() for page in Pages.query.filter(or_(Pages.access_mode==0, Pages.user_id==current_user.id)).all()])
	if data[0] == "new":
		page = Pages(user_id=current_user.id, name="New", access_mode=0, last_ver=datetime.now())
		db.session.add(page)
		db.session.commit()
		return page.serialize()

@usersBP.route("/account/<int:user_id>", methods=['GET', 'POST'])
@login_required
def account(user_id):
	if current_user.id != user_id:
		return redirect(url_for('main.pages'))
	else:
		user = User.query.filter_by(id=user_id).first_or_404()
		form = UpdateAccountForm()
		form.user_id = user_id
		if form.validate_on_submit():
			user.username = form.username.data
			user.email = form.email.data
			db.session.commit()
			flash('Account has been updated!', 'success')
			return redirect(url_for('users.account', user_id=user.id))
		elif request.method == 'GET':
			form.username.data = user.username
			form.email.data = user.email
		page = request.args.get('page', 1, type=int)
		return render_template('account.html', title='Account', user=user, form=form)

@usersBP.route("/register", methods=['GET', 'POST'])
def register():
	if current_user.is_authenticated:
		return redirect(url_for('main.pages'))
	form = RegistrationForm()
	if form.validate_on_submit():
		hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
		user = User(username=form.username.data, email=form.email.data, password=hashed_password)
		db.session.add(user)
		db.session.commit()
		flash('Account has been created successfully!', 'success')
		return redirect(url_for('users.register'))
	return render_template('register.html', title='Register', form=form)

@mainBP.route("/")
@usersBP.route("/login", methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('main.pages'))
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		if user and bcrypt.check_password_hash(user.password, form.password.data):
			login_user(user, remember=form.remember.data)
			next_page = request.args.get('next')
			user.last_seen = datetime.utcnow()
			db.session.commit()
			return redirect(next_page) if next_page else redirect(url_for('main.pages'))
		else:
			flash('Login Unsuccessful. Please check email and password', 'danger')
	return render_template('login.html', title='Login', form=form)

@usersBP.route("/logout")
def logout():
	logout_user()
	return redirect(url_for('users.login'))

@usersBP.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
	if current_user.is_authenticated:
		return redirect(url_for('main.homework'))
	form = RequestResetForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		send_reset_email(user)
		flash('An email has been sent with instructions to reset your password.', 'info')
		return redirect(url_for('users.login'))
	return render_template('reset_request.html', title='Reset Password', form=form)

@usersBP.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
	if current_user.is_authenticated:
		return redirect(url_for('main.homework'))
	user = User.verify_reset_token(token)
	if user is None:
		flash('That is an invalid or expired token', 'warning')
		return redirect(url_for('users.reset_request'))
	form = ResetPasswordForm()
	if form.validate_on_submit():
		hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
		user.password = hashed_password
		db.session.commit()
		flash('Your password has been updated! You are now able to log in', 'success')
		return redirect(url_for('users.login'))
	return render_template('reset_token.html', title='Reset Password', form=form)

@errorsBP.app_errorhandler(404)
def error_404(error):
	return render_template('errors/404.html'), 404

@errorsBP.app_errorhandler(403)
def error_403(error):
	return render_template('errors/403.html'), 403

@errorsBP.app_errorhandler(500)
def error_500(error):
	return render_template('errors/500.html'), 500
