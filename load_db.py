from flaskpage import db, create_app
from flaskpage.models import User, Homework, DoneHomework, Stats, Elements
from flaskpage.config import ConfigLH
from datetime import datetime
create_app(ConfigLH).app_context().push()
